package nl.utwente.mod4.pokemon;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

@ApplicationPath("/api")
public class PokeApp extends Application {
}